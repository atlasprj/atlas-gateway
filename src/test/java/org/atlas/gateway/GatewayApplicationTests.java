package org.atlas.gateway;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Test;

public class GatewayApplicationTests {

	@Test
	public void generalTests() throws ParseException {
		String strTime = "23:52:14";
		String target = LocalDate.now()+ " "+strTime;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date result =  df.parse(target);
		System.out.println(result.getTime());		
	}

}
