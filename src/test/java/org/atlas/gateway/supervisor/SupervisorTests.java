package org.atlas.gateway.supervisor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.atlas.gateway.utils.IpAddrUtils;
import org.atlas.gateway.utils.OsUtils;
import org.junit.Test;

public class SupervisorTests {

	@Test
	public void TopCommandParseTestsTest(){

		String topSample1 = "top - 23:52:14 up 27 min,  1 user,  load average: 0,79, 0,61, 0,54";
		SysGeneral sysGeneral1 = new SysGeneral(topSample1);
		assertEquals(sysGeneral1.getOsTime(), "23:52:14");
		assertEquals(sysGeneral1.getUpTime(), 1620);
		assertEquals(sysGeneral1.getConnectedUsers(), 1);
		assertEquals(sysGeneral1.getLastMinuteLoadAverage(), 0.79, 0.01);
		assertEquals(sysGeneral1.getLastFiveMinutesLoadAverage(), 0.61, 0.01);
		assertEquals(sysGeneral1.getLastFifteenMinutesLoadAverage(), 0.54, 0.01);
		
		String topSample2 = "top - 00:17:37 up 52 min,  2 users,  load average: 0,44, 0,41, 0,41";
		SysGeneral sysGeneral2 = new SysGeneral(topSample2);
		assertEquals(sysGeneral2.getOsTime(), "00:17:37");
		assertEquals(sysGeneral2.getUpTime(), 3120);
		assertEquals(sysGeneral2.getConnectedUsers(), 2);
		assertEquals(sysGeneral2.getLastMinuteLoadAverage(), 0.44, 0.01);
		assertEquals(sysGeneral2.getLastFiveMinutesLoadAverage(), 0.41, 0.01);
		assertEquals(sysGeneral2.getLastFifteenMinutesLoadAverage(), 0.41, 0.01);
		
		String topSample3 = "top - 00:51:28 up  1:26,  1 user,  load average: 0,66, 0,49, 0,37";
		SysGeneral sysGeneral3 = new SysGeneral(topSample3);
		assertEquals(sysGeneral3.getOsTime(), "00:51:28");
		assertEquals(sysGeneral3.getUpTime(), 5160);
		assertEquals(sysGeneral3.getConnectedUsers(), 1);
		assertEquals(sysGeneral3.getLastMinuteLoadAverage(), 0.66, 0.01);
		assertEquals(sysGeneral3.getLastFiveMinutesLoadAverage(), 0.49, 0.01);
		assertEquals(sysGeneral3.getLastFifteenMinutesLoadAverage(), 0.37, 0.01);
		
		String topSample4 = "top - 13:55:42 up 91 days, 15:05,  1 user,  load average: 0.05, 0.01, 0.00";
		SysGeneral sysGeneral4 = new SysGeneral(topSample4);
		assertEquals(sysGeneral4.getOsTime(), "13:55:42");
		assertEquals(sysGeneral4.getUpTime(), 7916700);
		assertEquals(sysGeneral4.getConnectedUsers(), 1);
		assertEquals(sysGeneral4.getLastMinuteLoadAverage(), 0.05, 0.01);
		assertEquals(sysGeneral4.getLastFiveMinutesLoadAverage(), 0.01, 0.01);
		assertEquals(sysGeneral4.getLastFifteenMinutesLoadAverage(), 0.00, 0.01);
		
		String tasksSample1 = "Tasks: 165 total,   1 running, 164 sleeping,   0 stopped,   0 zombie";
		SysTasks sysTasks1 = new SysTasks(tasksSample1);
		assertEquals(sysTasks1.getTotal(), 165);
		assertEquals(sysTasks1.getRunning(), 1);
		assertEquals(sysTasks1.getSleeping(), 164);
		assertEquals(sysTasks1.getStopped(), 0);
		assertEquals(sysTasks1.getZombie(), 0);
		
		String tasksSample2 = "Tasks: 163 total,   1 running, 162 sleeping,   0 stopped,   0 zombie";
		SysTasks sysTasks2 = new SysTasks(tasksSample2);
		assertEquals(sysTasks2.getTotal(), 163);
		assertEquals(sysTasks2.getRunning(), 1);
		assertEquals(sysTasks2.getSleeping(), 162);
		assertEquals(sysTasks2.getStopped(), 0);
		assertEquals(sysTasks2.getZombie(), 0);
		
		String tasksSample3 = "Tasks: 163 total,   1 running, 162 sleeping,   0 stopped,   0 zombie";
		SysTasks sysTasks3 = new SysTasks(tasksSample3);
		assertEquals(sysTasks3.getTotal(), 163);
		assertEquals(sysTasks3.getRunning(), 1);
		assertEquals(sysTasks3.getSleeping(), 162);
		assertEquals(sysTasks3.getStopped(), 0);
		assertEquals(sysTasks3.getZombie(), 0);
		
		String cpuSample1 = "%Cpu(s): 22,7 us,  1,6 sy,  0,0 ni, 75,5 id,  0,2 wa,  0,0 hi,  0,1 si,  0,0 st";
		SysCpus	sysCpu1 = new SysCpus(cpuSample1);
		assertEquals(sysCpu1.getUs(), 22.7, 0.01);
		assertEquals(sysCpu1.getSy(), 1.6, 0.01);
		assertEquals(sysCpu1.getNi(), 0.0, 0.01);
		assertEquals(sysCpu1.getNu(), 75.5, 0.01);
		assertEquals(sysCpu1.getWa(), 0.2, 0.01);
		assertEquals(sysCpu1.getHi(), 0.0, 0.01);
		assertEquals(sysCpu1.getSi(), 0.1, 0.01);
		assertEquals(sysCpu1.getSt(), 0.0, 0.01);
		
		String cpuSample2 = "%Cpu(s): 16,6 us,  1,3 sy,  0,0 ni, 81,9 id,  0,1 wa,  0,0 hi,  0,1 si,  0,0 st";
		SysCpus	sysCpu2 = new SysCpus(cpuSample2);
		assertEquals(sysCpu2.getUs(), 16.6, 0.01);
		assertEquals(sysCpu2.getSy(), 1.3, 0.01);
		assertEquals(sysCpu2.getNi(), 0.0, 0.01);
		assertEquals(sysCpu2.getNu(), 81.9, 0.01);
		assertEquals(sysCpu2.getWa(), 0.1, 0.01);
		assertEquals(sysCpu2.getHi(), 0.0, 0.01);
		assertEquals(sysCpu2.getSi(), 0.1, 0.01);
		assertEquals(sysCpu2.getSt(), 0.0, 0.01);
		
		String cpuSample3 = "%Cpu(s): 13,8 us,  0,9 sy,  0,0 ni, 85,1 id,  0,1 wa,  0,0 hi,  0,0 si,  0,0 st";
		SysCpus	sysCpu3 = new SysCpus(cpuSample3);
		assertEquals(sysCpu3.getUs(), 13.8, 0.01);
		assertEquals(sysCpu3.getSy(), 0.9, 0.01);
		assertEquals(sysCpu3.getNi(), 0.0, 0.01);
		assertEquals(sysCpu3.getNu(), 85.1, 0.01);
		assertEquals(sysCpu3.getWa(), 0.1, 0.01);
		assertEquals(sysCpu3.getHi(), 0.0, 0.01);
		assertEquals(sysCpu3.getSi(), 0.0, 0.01);
		assertEquals(sysCpu3.getSt(), 0.0, 0.01);
	}
	
	@Test
	public void LocalIpAddressTests(){
		HashMap<String, String> ips = IpAddrUtils.getLocalIpAddress();
		Iterator it = ips.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.println(pair.getKey() + " = " + pair.getValue());
	    }
	}
	
	@Test
	public void CheckingEnvironmentalVariable(){
		String varbl1 = OsUtils.getEnviromentalVariable("USER");
		System.out.println(varbl1);
		String varbl2 = OsUtils.getEnviromentalVariable("KOSNASSA");
		assertNull(varbl2);
	}
	
}
