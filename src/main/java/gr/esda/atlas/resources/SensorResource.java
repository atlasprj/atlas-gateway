package gr.esda.atlas.resources;

import java.io.IOException;

import org.atlas.wsn.devices.SensorService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SensorResource {

	private int id;
	private String address;
	private String configuration;
	private String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getConfiguration() {
		return configuration;
	}
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public SensorService[] getServices() throws JsonParseException, JsonMappingException, IOException{
		return new ObjectMapper().readValue(this.getConfiguration(), SensorService[].class);
	}
	
}
