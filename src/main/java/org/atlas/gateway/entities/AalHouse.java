package org.atlas.gateway.entities;

public class AalHouse{
	
	private int id;
	private String location;
	private String identity;
	private String address;
	private double latitude;
	private double longitude;
	private AalHouseStatus status;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public AalHouseStatus getStatus() {
		return status;
	}

	public void setStatus(AalHouseStatus status) {
		this.status = status;
	}

	public enum AalHouseStatus {
		
		ACTIVE,
		
		INACTIVE,
		
		DELETED;

	}

}
