package org.atlas.gateway.services;

import java.util.List;

import org.atlas.gateway.components.database.models.Route;

public interface Router {

	public Route getDestination(String source);
	public List<String> getBridges();
	
}
