package org.atlas.gateway.components.wsn;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.atlas.gateway.components.GatewayFactory;
import org.atlas.gateway.components.database.Database;
import org.atlas.gateway.components.database.models.Unity;
import org.atlas.gateway.connectors.SysLocalConnector;
import org.atlas.gateway.utils.OsUtils;
import org.atlas.gateway.utils.OsUtils.OSType;
import org.atlas.wsn.ble.BluetoothAdapter;
import org.atlas.wsn.ble.impl.BluetoothServiceImpl;
import org.atlas.wsn.ble.listener.BluetoothAdvertisementData;
import org.atlas.wsn.ble.listener.BluetoothAdvertisementScanListener;
import org.atlas.wsn.devices.SensorDevice;
import org.atlas.wsn.handlers.BleWatchDog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;

import gr.esda.atlas.messages.AtlasMessages.Advertisment;
import gr.esda.atlas.messages.AtlasMessages.Advertisment.WirelessTechnology;
import gr.esda.atlas.resources.SensorResource;

@Component
public class WSNHandler implements BluetoothAdvertisementScanListener {

	private static final Logger logger = LoggerFactory.getLogger(WSNHandler.class);
	private BluetoothAdapter bleAdapter;
	private ExecutorService connectedDevicesExecutor;
	private long lastAdvertismentReceived = 0;//In milliseconds
	private long threshold = 10000;//In milliseconds
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	private HashMap<String, SensorDevice> bleDevices;
	private HashMap<String, SensorResource> gwKnownDevices;
	
	@Autowired
	private GatewayFactory factory;
	
	@Autowired
	private Database database;
	
	@PostConstruct
	public void bootUpWSNHandler(){
		this.bleDevices = new HashMap<String, SensorDevice>();
		this.gwKnownDevices = new HashMap<String, SensorResource>();
		
		logger.info("Loading devices for gateway["+factory.getGatewayId()+"] from ATLAS Resources application...");
		this.getGatewaySensors();
		
		if( OsUtils.getOperatingSystemType() != OSType.Linux ) {
			logger.warn("Operating system not supported.");
			return;
		}
		
		BluetoothServiceImpl bleService = new BluetoothServiceImpl();
		bleAdapter = bleService.getBluetoothAdapter();
		if( bleAdapter != null ){
			logger.info("Local bluetooth device address: "+bleAdapter.getAddress());
			if( !bleAdapter.isEnabled() ){
				logger.info("BLE Adapter is not enabled, going to enable it...");
				bleAdapter.enable();
				logger.info("BLE Adapter enabled succefully...");
			}
			logger.info("Starting advertisements scanning...");
			ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			this.connectedDevicesExecutor = Executors.newFixedThreadPool(7);//Because of maxixum connected devices.
			scheduler.scheduleAtFixedRate(new Runnable() {
				
				@Override
				public void run() {
					logger.info("Checking Advertismemtns Handler status....");
					enableAdvertismentsHandler();
					checkDevicesConnection();
				}
			}, 5, 12, TimeUnit.SECONDS);
         }else{
        	 logger.warn("Unable to find local BLE Adapter");
         } 
	}	
	
	private void getGatewaySensors() {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(factory.getSensorsUrl());
		try {
			HttpResponse response = client.execute(request);
			if( response.getStatusLine().getStatusCode() == HttpStatus.SC_OK ){
				logger.info("Received successfully sensors from ATLAS");
				SensorResource[] gwSensorsLst = jsonMapper.readValue(response.getEntity().getContent(), SensorResource[].class);
				for( SensorResource res:  gwSensorsLst ){
					logger.info("Add sensor["+res.getAddress()+"] to gateway allowed list.");
					this.gwKnownDevices .put(res.getAddress(), res);
				}
				
			}
		} catch (IOException e) {
			logger.error("Unable to load gateway sensors.",e);
		}
		
	}

	@SuppressWarnings("rawtypes")
	private void checkDevicesConnection() {
		Iterator it = this.bleDevices.entrySet().iterator();
		while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        SensorDevice device = (SensorDevice) pair.getValue();
			if( device.isConnectable() ){
				long lstMsg = BleWatchDog.getLastMsgFromDevice(device.getDevice().getAdress());
				if( lstMsg > 30000 ) it.remove();
			}
	    }
	}
	
	private void enableAdvertismentsHandler(){
		try {
			if( this.bleAdapter.isScanning() ){
				//Reason unknown scanning is stopped.
				if( (Calendar.getInstance().getTimeInMillis() - this.lastAdvertismentReceived) > this.threshold ){
					logger.info("Not received advertisments for "+this.threshold+" milliseconds, re-start, Last packet: " + new Date(this.lastAdvertismentReceived));
					Runtime.getRuntime().exec("killall hcidump");
					this.bleAdapter.startAdvertisementScan("000D", this);
				}
			}else{
				logger.info("Not scanning for advertisment, enable them.");
				Runtime.getRuntime().exec("killall hcidump");
				this.bleAdapter.startAdvertisementScan("000D", this);
			}
		}catch (IOException e) {
			logger.error("Unable to exec Runtime.exec --> hcidump");
		}
	}

	@Override
	public void onAdvertisementDataReceived(BluetoothAdvertisementData btAdData) {
		if( btAdData.getReportRecords().size() == 0 ) return;
		String deviceAddress = btAdData.getReportRecords().get(0).getAddress();
		this.lastAdvertismentReceived = Calendar.getInstance().getTimeInMillis();
		logger.info("Message received from device address: " + deviceAddress);
		
		switch( btAdData.getReportRecords().get(0).getEventType() ){
			case 0:
				logger.info("Packet Type: ADV_IND, Submitting request for connecting with the device.");
				SensorResource knownResourceSensor = this.gwKnownDevices.get(deviceAddress.replaceAll(":", ""));
				if( (knownResourceSensor != null) && (this.bleDevices.get(deviceAddress) == null) ){
					SensorDevice device = new SensorDevice(deviceAddress,true, getUnityMap(deviceAddress), knownResourceSensor);
					this.bleDevices.put(deviceAddress, device);
					this.connectedDevicesExecutor.execute(device);
				}
				break;
			
			case 1:
				logger.info("Packet Type: ADV_DIRECT_IND, Ignoring");
				break;
			
			case 2:
				logger.info("Packet Type: ADV_SCAN_IND, Ignoring...");
				break;
				
			case 3:
				logger.info("Packet Type: ADV_NONCONN_IND, Forwarding messages to Topic...");
				Advertisment advertisment = Advertisment
						.newBuilder()
						.setAddress(deviceAddress)
						.setTechnology(WirelessTechnology.BLE)
						.setData(ByteString.copyFrom(btAdData.getRawData()))
						.build();
				SysLocalConnector.publish("wsn/ble/devices/advertisments", advertisment.toByteArray(),1);
				break;
				
			case 4:
				logger.info("Packet Type: SCAN_RSP, Ignoring...");
				break;
				
			default:
				logger.warn("Unimplemented...");
		
		}
		
	}
	
	@PreDestroy
	public void shutDownHandler(){}
	
	private int getUnityMap(String address) {
		ResultSetHandler<Unity> h = new BeanHandler<Unity>(Unity.class);
		try {
			Unity model = database.getSQLRunner().query("SELECT * FROM unity WHERE address=?", h, address);
			if( model == null ) return -1;
				return model.getSensorId();
			} catch (SQLException e) {
				logger.error("Unable to retrieve device...",e);
			}
		return -1;
	}
	
}
