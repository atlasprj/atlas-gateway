package org.atlas.gateway.components;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.atlas.gateway.components.cloud.CloudHandler;
import org.atlas.gateway.components.mediator.Mediator;
import org.atlas.gateway.entities.AalHouse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@SuppressWarnings("unused")
public class GatewayFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(GatewayFactory.class);
	
	@Autowired
	private Mediator mediator;

//	@Autowired
//	private CloudHandler cloudHandler;
//	
	@Value("${atlas.resource.base.url}")
	private String baseUrl;
	
	@Value("${atlas.gw.identity}")
	private String identity;
	
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	private int gatewayId;
	private AalHouse aalhouse;
	
	@PostConstruct
	public void init() {
		HttpClient client = HttpClientBuilder.create().build();
		logger.info("Fetching gateway identity ["+baseUrl + "gateways/"+identity+"]");
		HttpGet request = new HttpGet(baseUrl + "gateways/"+identity);
		JsonNode accountTree;
		try {
			HttpResponse response = client.execute(request);
			accountTree = this.jsonMapper.readTree(response.getEntity().getContent());
			this.gatewayId = accountTree.at("/id").asInt();
			logger.info("Gateway identity [Primary Key] - " + this.gatewayId);
		} catch (IOException e) {
			logger.error("Unable to perform gateway id parse.",e);
		}
		
		
		HttpGet aalRequest = new HttpGet(baseUrl + "gateways/"+this.gatewayId+"/aalhouse");
		logger.info("Fetching gateway aalhouse ["+baseUrl + "gateways/"+this.gatewayId+"/aalhouse"+"]");
		try {
			HttpResponse response = client.execute(aalRequest);
			this.aalhouse = this.jsonMapper.readValue(response.getEntity().getContent(), AalHouse.class);
		} catch (IOException e) {
			logger.error("Unable to retrieve gateway aalhouse.",e);
		}
		
		
	}
	
	/**
	 * Request from atlas the gateway id.
	 * @return the id
	 */
	public int getGatewayId() {
		return this.gatewayId;
	}

	public AalHouse getAalhouse() {
		return aalhouse;
	}

	public void setAalhouse(AalHouse aalhouse) {
		this.aalhouse = aalhouse;
	}
	
	public String getSensorsUrl() {
		return baseUrl + "gateways/"+this.gatewayId+"/sensors";
	}
	
}
