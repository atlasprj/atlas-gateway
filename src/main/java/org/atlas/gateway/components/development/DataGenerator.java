package org.atlas.gateway.components.development;

import javax.annotation.PostConstruct;

import org.atlas.gateway.components.cloud.DataPublisher;
import org.atlas.wsn.devices.ServiceData;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DataGenerator {
	
	@PostConstruct
	public void init() {		
	}
	
	
	@Scheduled(fixedRate = 250)
	public void scheduleFixedRateTask() {
		System.out.println("Data Generator - scheduleFixedRateTask");
//		ServiceData data = new ServiceData("247189073180", "light", 10.10);
//		data.setPayload("data,1,light,10.10");
//		DataPublisher.addData(data);
	}

}
